# ics-ans-role-juniper-vlan

Ansible role to add new vlans to juniper switches based on network names. Note juniper_vlan_vlans are case sensitive. 
And the network being added needs to have a prior host being in the same vlan. Due to this role searching through all hostvars for finding corresponding
vlan name and id.

## Role Variables

```yaml
juniper_vlan_vlans: []
# - test
# - foo
# - fake
# - [append, another]
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-juniper-vlan
```

## License

BSD 2-clause
