def get_vlan_id(hostnames, hostvars):
    result = []
    for hostname in hostnames:
        names = compare_names(hostname, hostvars)
        if names is not None:
            result.extend(names)
    return result


def compare_names(hostname, hostvars):
    result = []
    for hostvar in hostvars.values():
        for interface in hostvar.get("csentry_interfaces", []):
            network = interface.get("network")
            vlan_id = network.get("vlan_id", None)
            name = network.get("name", None)
            if name == hostname:
                result.append({"name": name, "vlan_id": vlan_id})
                return result
    return None


class FilterModule(object):
    def filters(self):
        return {"get_vlan_id": get_vlan_id}
